cmd_mariadb_help() {
    cat <<_EOF
    mariadb [ create | drop | dump | sql | script ]
        Manage the database of the container '$CONTAINER'.
        - create
              create the database '$DBNAME'
        - drop
              drop the database '$DBNAME'
        - dump
              dump the database '$DBNAME'
        - sql <query>
              run the given sql query on '$DBNAME'
        - script <file.sql>
              run the given sql script on '$DBNAME'

_EOF
}

cmd_mariadb() {
    [[ -n $DBHOST ]] || fail "Error: No DBHOST defined on 'settings.sh'"

    local cmd=$1
    shift
    case $cmd in
        sql)
            [[ -n $1 ]] || fail "Usage:\n$(cmd_mariadb_help)"
            podman exec $DBHOST mysql -e "$1"
            ;;
        drop)
            podman exec $DBHOST mysql -e "drop database if exists $DBNAME"
            ;;
        create)
            podman exec $DBHOST mysql -e "
                create database if not exists $DBNAME;
                grant all privileges on $DBNAME.* to '$DBUSER'@'$CONTAINER.$NETWORK' identified by '$DBPASS';
                flush privileges; "
            podman restart $DBHOST
            sleep 3
            ;;
        dump)
            podman exec $DBHOST mysqldump --allow-keywords --opt $DBNAME
            ;;
        script)
            [[ -n $1 ]] || fail "Usage:\n$(cmd_mariadb_help)"
            sqlfile=$1
            podman cp $sqlfile $DBHOST:/tmp/
            podman exec $DBHOST sh -c "mysql --database=$DBNAME < /tmp/$(basename $sqlfile)"
            ;;
        *)
            echo -e "Usage:\n$(cmd_mariadb_help)"
            exit
            ;;
    esac
}
