# MariaDB in a Container

## Installation

  - First install `cs`: https://gitlab.com/container-scripts/cs#installation

  - Then get the scripts: `cs pull mariadb`

  - Create a directory for the container: `cs init mariadb @mariadb`

  - Fix the settings: `cd /var/cs/mariadb/ && vim settings.sh`

  - Build image, create the container and configure it: `cs make`
